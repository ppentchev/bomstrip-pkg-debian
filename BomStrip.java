public class BomStrip {
	
	private static final int BUFFER_LENGTH = 65536;
	private static final int EOF = -1;
	private static final byte UTF8_BOM_1 = (byte) 0xef;
	private static final byte UTF8_BOM_2 = (byte) 0xbb;
	private static final byte UTF8_BOM_3 = (byte) 0xbf;
	
	
	public static void main(String[] args) {
		final byte[] utf8Bom = {UTF8_BOM_1, UTF8_BOM_2, UTF8_BOM_3};
		byte[] buffer = new byte[BUFFER_LENGTH];	
		byte[] bomBuffer = new byte[utf8Bom.length];

		try {
			int nRead = System.in.read(bomBuffer, 0, bomBuffer.length);
			if (nRead != EOF) {
				if (!java.util.Arrays.equals(bomBuffer, utf8Bom)) {
					System.out.write(bomBuffer, 0, nRead);
				}
				boolean eof = false;
				while (!eof) {
					nRead = System.in.read(buffer, 0, buffer.length);
					eof = nRead == EOF;
					if (!eof) {
						System.out.write(buffer, 0, nRead);
					}	
				}
			}
		}
		catch (java.io.IOException e)
		{
			System.err.println("I/O error occurred: " + e.getMessage());
			System.exit(1);	
		}
	}
}
