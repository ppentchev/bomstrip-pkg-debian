implement Bomstrip;

include "sys.m";
include "draw.m";

Bomstrip: module
{
	init:	fn(ctxt: ref Draw->Context, argv: list of string);
};

init(nil: ref Draw->Context, nil: list of string)
{
	sys := load Sys Sys->PATH;
	buf := array[sys->ATOMICIO] of byte;
	utf8bom := array[3] of {byte 16rEF, byte 16rBB, byte 16rBF};
	stdin := sys->fildes(0);
	stdout := sys->fildes(1);

	n := sys->read(stdin, buf, len buf);
	if(n < 0)
		raise "fail:read";
	if(n == 0)
		return;
	m := 3;
	if(n < m)
		m = n;
	if(m != 3
		|| buf[0] != utf8bom[0]
		|| buf[1] != utf8bom[1]
		|| buf[2] != utf8bom[2])
		sys->write(stdout, buf[0:3], m);
	if(n > 3)
		sys->write(stdout, buf[3:n], n-3);

	for(;;) {
		n = sys->read(stdin, buf, len buf);
		if(n < 0)
			raise "fail:read";
		if(n == 0)
			break;
		sys->write(stdout, buf, n);
	}
}
