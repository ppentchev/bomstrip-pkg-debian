bomstrip (9-17) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.7.0 with no changes.
  * Bump the debhelper compat level to 14 and let debhelper add some
    default dependencies automatically.
  * Use dpkg's default.mk in the rules file.
  * Declare dpkg-build-api v1, drop the implied Rules-Requires-Root: no.

 -- Peter Pentchev <roam@debian.org>  Wed, 03 Jul 2024 20:36:51 +0300

bomstrip (9-16) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.2 with no changes.
  * Add ELF package metadata

 -- Peter Pentchev <roam@debian.org>  Fri, 14 Jul 2023 16:17:07 +0300

bomstrip (9-15) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.0 with no changes.
  * Explicitly specify dh-sequence-single-binary in the list of build
    dependencies.
  * Add the constify patch to mark some C/C++ variables as constant.
  * Update the upstream author's e-mail address.
  * Mark two patches as forwarded upstream.

 -- Peter Pentchev <roam@debian.org>  Mon, 15 Nov 2021 12:52:44 +0200

bomstrip (9-14) unstable; urgency=medium

  * Drop the adequate autopkgtest.
  * Drop the Name and Contact upstream metadata fields.
  * Update to debhelper compat level 13:
    - drop the conditional around the override_dh_auto_test target

 -- Peter Pentchev <roam@debian.org>  Sat, 02 May 2020 14:59:23 +0300

bomstrip (9-13) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.5.0 with no changes.
  * Use the test-name autopkgtest feature.
  * Mark the adequate autopkgtest as superficial.
  * Do not use the host compiler when cross-building. (Closes: #951740)
  * Add a trivial git-buildpackage config file.

 -- Peter Pentchev <roam@debian.org>  Tue, 25 Feb 2020 17:41:03 +0200

bomstrip (9-12) unstable; urgency=medium

  * Use my Debian e-mail address.
  * Declare compliance with Debian Policy 4.3.0 with no changes.
  * Use the B-D: debhelper-compat (= 11) mechanism.
  * Add a trivial autopkgtest running adequate on the installed package.
  * Move away from git-dpm.
  * Bump the debhelper compatibility level to 12 with no changes.

 -- Peter Pentchev <roam@debian.org>  Wed, 26 Dec 2018 01:33:57 +0200

bomstrip (9-11) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.1.3 and drop the implied
    "Testsuite: autopkgtest" source control field.
  * Switch to git-dpm and rename the patches.
  * Add "Rules-Requires-Root: no" to the source control stanza.
  * Bump the debhelper compatibility level to 11 with no changes.

 -- Peter Pentchev <roam@ringlet.net>  Sun, 21 Jan 2018 01:38:21 +0200

bomstrip (9-10) unstable; urgency=medium

  * Build-depend on debhelper 10 now that it's even in jessie-backports;
    remove the Lintian override.
  * Correct the upstream site location in the debian/bomstrip.1 manual
    page, too.
  * Use the HTTPS scheme for various Debian and upstream URLs.
  * Add the 02_typos patch to correct a typographical error.

 -- Peter Pentchev <roam@ringlet.net>  Mon, 09 Jan 2017 22:03:24 +0200

bomstrip (9-9) unstable; urgency=medium

  * Declare compliance with Debian Policy 3.9.8 with no changes.
  * Drop the versioned dependency on dpkg-dev, it's satisfied everywhere.
  * Update the watch file to version 4 and explicitly specify pgpmode=none.
  * Add an autopkgtest suite.
  * Bump the debhelper compatibility level to 10:
    - override the Lintian debhelper version warning as it itself suggests
    - let debhelper handle parallel building

 -- Peter Pentchev <roam@ringlet.net>  Thu, 21 Apr 2016 23:28:15 +0300

bomstrip (9-8) unstable; urgency=medium

  * Switch Vcs-Git and Vcs-Browser to my full-source GitLab repository.
  * Declare compliance with version 3.9.6 of the Debian Policy, no changes.
  * Get the build hardening flags directly from debhelper.
  * Add an upstream metadata file.

 -- Peter Pentchev <roam@ringlet.net>  Sat, 05 Sep 2015 17:23:16 +0300

bomstrip (9-7) unstable; urgency=low

  * Remove the DM-Upload-Allowed flag since Damyan Ivanov was kind
    enough to grant me permission using the new dak mechanism.
  * Bump Standards-Version to 3.9.5 with no changes.
  * Update the copyright file to the copyright-format/1.0 format.
  * Bump the debhelper compatibility version to 9 with no changes.
  * Get the hardening options directly from dpkg-buildflags:
    - bump the build dependency on dpkg-dev to 1.16.1~
    - remove the build dependency on hardening-includes
    - no longer include the hardening Makefile snippet into the rules file
    - explicitly enable all the hardening features; they may be disabled
      in the future if bomstrip should fail to build anywhere
  * Enable parallel building - not that it matters a lot in this case :)
  * Use DEB_CFLAGS_MAINT_APPEND to append the warning compiler flags.
  * Add _POSIX_C_SOURCE and _XOPEN_SOURCE specifications, just in case.
  * Drop the explicit compression specification for the Debian tarball;
    it really does not matter for this package.

 -- Peter Pentchev <roam@ringlet.net>  Sun, 17 Nov 2013 14:52:55 +0200

bomstrip (9-6) unstable; urgency=low

  * Update the copyright file to the latest DEP 5 candidate format.
  * Bump Standards-Version to 3.9.2 with no changes.
  * Update the copyright file to the latest DEP 5 candidate format and
    fix the DEP 5 URL after the Alioth migration.
  * Specify Multi-Arch: foreign for the binary package, just in case.

 -- Peter Pentchev <roam@ringlet.net>  Thu, 14 Jul 2011 19:04:00 +0300

bomstrip (9-5) unstable; urgency=low

  * The upstream site has moved, update the Homepage field, the watch file
    and the copyright file.
  * Update the copyright file to rev. 166 of the DEP 5 candidate.
  * Upload to unstable.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 08 Feb 2011 16:14:12 +0200

bomstrip (9-4) experimental; urgency=low

  * Switch to Git and point the Vcs-* fields to Gitorious.
  * We haven't been using dpatch for more than two years, so drop
    the executable permissions on the patch file.
  * Bump Standards-Version to 3.9.1 with no changes.
  * Switch to bzip2 compression for the Debian tarball.
  * Depend on a hardening package so that the build hardening is actually
    done in the automated builds, and use the hardening-includes package
    instead of hardening-wrapper so that the hardening flags are visible in
    CFLAGS and LDFLAGS.
  * Bump the debhelper compatibility level to 8 with no changes.

 -- Peter Pentchev <roam@ringlet.net>  Fri, 24 Dec 2010 15:02:51 +0200

bomstrip (9-3) unstable; urgency=low

  * Bump Standards-Version to 3.9.0:
    - honor the "nocheck" build option and do not run dh_auto_test anyway
  * Simplify the rules file:
    - simplify the DEB_BUILD_HARDENING logic
    - move various dh_* command parameters out into separate debian/* files
    - use debhelper override rules
  * Fix a rules file bug: use LDFLAGS, not CFLAGS, to link a C program.
  * Use dpkg-buildflags from dpkg-dev 1.15.7 to obtain default values for
    CFLAGS, CPPFLAGS, and LDFLAGS.
  * Convert to the 3.0 (quilt) source format.
  * Convert the copyright file to the latest DEP 5 format.
  * Convert the single patch's header to the DEP 3 format.
  * Shorten the Vcs-Browser URL and actually point it at the Debian package.
  * Honor CPPFLAGS.
  * Add DM-Upload-Allowed with Damyan Ivanov's permission.

 -- Peter Pentchev <roam@ringlet.net>  Wed, 30 Jun 2010 08:35:48 +0300

bomstrip (9-2) unstable; urgency=low

  * Fix the short description.  Closes: #489795
  * Switch to quilt as Damyan Ivanov (dmn) suggested.
  * Fix some of the checks in my "C warnings fix" patch
  * Add two checks reported by the Debian hardening wrapper
  * Enable build hardening unless DEB_BUILD_OPTIONS contains "nohardening"
  * Use debhelper 7's features to minimize the rules file
  * Spell "Public Domain" as "PD", not "other", in the copyright file

  * debian/README.source
    - describe the usage of quilt, not dpatch
  * debian/changelog
    - use the "PD" name for the public domain license
  * debian/control
    - fix the short description.  Closes: #489795
    - build-depend on quilt, at least 0.40 for quilt.make
  * debian/patches/series
    - renamed from dpatch's 00list
  * debian/patches/01_c_warnings.patch
    - renamed from dpatch's 01_c_warnings.dpatch
    - refresh the patch with -p ab --no-index --no-timestamps
    - rewrite the header, dropping the dpatch-style comments
    - check the fwrite() return code for errors, found by the Debian
      hardening wrapper
    - restore the size_t type of nread and fix the signedness warning
      the right way - fread() may never return a negative value, but
      if it should returns zero, check for ferror()
  * debian/rules
    - use quilt.make and ${QUILT_STAMPFN} instead of dpatch's ones
    - enable build hardening unless DEB_BUILD_OPTIONS contains "nohardening"
    - use debhelper 7's "dh" wrapper to do most of the work

 -- Peter Pentchev <roam@ringlet.net>  Wed, 09 Jul 2008 13:40:31 +0300

bomstrip (9-1) unstable; urgency=low

  * Initial release.  Closes: #486425

 -- Peter Pentchev <roam@ringlet.net>  Fri, 27 Jun 2008 12:38:16 +0300
