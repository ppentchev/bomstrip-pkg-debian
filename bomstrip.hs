-- by mechiel lukkien, 18 september 2005, public domain

putStrNoBOM :: String -> IO ()
putStrNoBOM ('\xef':'\xbb':'\xbf':s) = putStr s
putStrNoBOM s = putStr s

main :: IO ()
main = do s <- getContents
	  putStrNoBOM s
