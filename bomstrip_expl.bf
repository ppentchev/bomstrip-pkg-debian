Berteun Damman; 2005; Public Domain; 

Read the first three bytes; the BOM marker (if it's there)

The marker; in case it's there it is 0xEF 0xBB 0xBF (239 187 191)

Read a byte of input; if it's minus 1 we treat it as an EOF
,+ [ -
	So it's not an EOF and we make two copies of it
	[>>+<+<-]>>
	And we put our constant after it; as the number 239 itself 
	is a prime we use 240 which factorizes nicely as 2^4 * 3 * 5
	and substract one from it
	>>>>+++++[<++++>-]<[<++++>-]<[<+++>-]<-

	While our constant is not zero we substract one from it and also
	from the input byte
	[<->-]

	Now change our constant to 1 (our flag)
	+
	Check whether the input byte is zero; if so we have a match
	< [ 
		It was non zero we go to our flag and turn it back
		to zero
		>->>>++++

		As the input byte might be negative (or wrapped! but in that case this won't hurt)
		we're going to add 256 which will make it positive for sure
		[<++++[<++++[<++++[<+>-]>-]>-]>-]
		<<<<
		[-]
	] > [ -
		If we get here the flag was non zero
		We now basically do the same but for the second byte
		,+  [ -
			So it's not an EOF and we make two copies of it
			[>>+<+<-]>>
		
			And we put our constant again next to it; 
			We first calculate 189; which is 3^3 * 7 and substract 2
			>>>>+++++++[<+++>-]<[<+++>-]<[<+++>-]<-- The number 0xBB


			While our constant is not zero we substract one from it and also
			from the input byte

			[<->-]

			Now change our constant to 1 (our flag)
			+
			Check whether the input byte is zero; if so we have
			a match
			< [
				It was non zero we go to our flag and turn it back to zero and add in case of negative
				>->>>++++
				[<++++[<++++[<++++[<+>-]>-]>-]>-]
				<<<<
				[-]
			] > [ -
				,+ [ - 
					And the whole story again
					[>>+<+<-]>>
					>>>>+++++++[<+++>-]<[<+++>-]<[<+++>-]<++ The number 0xBB
					[<->-]
					+
					< [ 
						>->>>++++
						[<++++[<++++[<++++[<+>-]>-]>-]>-]
						<<<<
						[-]
						
					] > [ -
						Evil Bom Found: clear the inputs
						<<[-]<<<[-]<<<[-]
						>>>>>>>>
					] Third byte test failed!
					<<<
				] End of file at third byte
			] Second  byte test failed
			<<<
		] End of file at second byte
	] First  byte test failed
	<<<
] End of file at first byte
If we broke out of the loops somewhere that means that we have to print the inputs on 
the tape; we do this by going from left to right and at the positions where the 
input should be we print it if it is non zero
When the BOM is found the whole tape has been made blank and we do not print anything
>[.>]>>[.>]>>[.>]

Print the remainder
,+[-.,+]
