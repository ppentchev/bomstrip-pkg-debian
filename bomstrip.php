#!/usr/bin/env php 
<?php // written by Andrew Gerrand nf@wh3rd.net http://nf.wh3rd.net/
      // this program is in the public domain

$fp = fopen('php://stdin','r');
$str = fread($fp, 3);
if ($str != "\xef\xbb\xbf")
    printf("%s",$str);
fpassthru($fp);
fclose($fp);

?>
