#!/usr/bin/perl

my $buf;
if (read STDIN, $buf, 3) {
	print $buf if $buf ne "\xef\xbb\xbf";
	undef $/;
	print <STDIN>;
}
