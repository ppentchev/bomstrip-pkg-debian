#!/bin/sh

test -z "$BOM" && BOM='ocaml ./bomstrip.ocaml'
test -z "$BOMFILES" && BOMFILES='bom0 bom1 nobom0 nobom1 nobom2'

res=0
for f in $BOMFILES; do
	$BOM < "$f" > "r$f"
	if ! cmp "r$f" correct/"r$f"; then
		echo "$f is wrong"
		res=1
	else
		rm -f "r$f"
	fi
done
exit "$res"
