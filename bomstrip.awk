#!/usr/bin/awk -f
# by Peter Pentchev, 2008, public domain.
# does not work on the one true awk, does work on the gnu clone.  but still then fails if file does not end in newline.

NR == 1 && /^﻿/ { sub("^...", "") }
{ print }
