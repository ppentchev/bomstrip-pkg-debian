# this only works with some seds.
# lines without newline should get a newline after outputting.
# (strange, but that's how sed is supposed to work).
# actually, it seems sed input "should" always have a newline at
# the end of a file.

# it is know to work with:
# - sed on openbsd 3.7
# - sed on debian (probably gnu sed)

# it is know to not work with:
# - sun os (ignores "trailing text without newline" all together)
# - mac os and plan 9 (their sed print newlines after every "line"
#   of input, even when it does not have a newline).  since mac os
#   sed seems to come from freebsd, it is expected that freebsd sed
#   also does not work.

# too bad...

1s/^﻿//
