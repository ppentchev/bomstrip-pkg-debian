/* by Peter Pentchev, 2008, public domain. */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

const char	*utf8bom = "\xef\xbb\xbf";

static void	 usage(const char *);
static void	 outendl(string &);

static void
usage(const char *prog)
{
	cerr << "usage: " << prog << endl;
	exit(1);
}

static void
outendl(string &s)
{
	cout << s;
	if (!cin.eof())
		cout << endl;
}

int
main(int argc, const char * const argv[])
{
	string s;

	if (argc > 1)
		usage(argv[0]);

	/* Empty? */
	if (!getline(cin, s))
		return 0;

	/* First line... */
	if (!s.substr(0, 3).compare(utf8bom))
		s = s.substr(3);
	outendl(s);

	/* ...and the rest. */
	while (getline(cin, s))
		outendl(s);
}
