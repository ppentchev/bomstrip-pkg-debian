#!/usr/bin/env python2.3

"""
Strip leading byte-order-mark from utf-8 files.
"""

import sys

def usage(prog):
	print >>sys.stderr, 'usage: %s' % prog
	sys.exit(1)

def main(prog, *args):
	bufsize = 65536
	utf8bom = '\xef\xbb\xbf'

	from getopt import getopt, GetoptError
	try:
		opts, args = getopt(args, '')
	except GetoptError:
		usage(prog)

	if args:
		usage(prog)

	inf = sys.stdin
	outf = sys.stdout

	buf = inf.read(len(utf8bom))
	if buf != utf8bom:
		outf.write(buf)
	if buf == '':
		return

	while True:
		buf = inf.read(bufsize)
		if buf == '':
			break
		outf.write(buf)
	
if __name__ == '__main__':
	try:
		main(*sys.argv)
	except KeyboardInterrupt:
		sys.exit(1)
